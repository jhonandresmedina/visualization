from django.db import models

# Create your models here.


class Autors(models.Model):
    name = models.CharField(max_length=220)
    published_articles = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name, self.published_articles
