from django.template import RequestContext
from django.shortcuts import render_to_response
from john.models import Autors
from django.http import HttpResponse
import csv


def consolidado(request):

    # autores = Autors.objects.order_by('name')
    # autores = Autors.objects.values_list('name', 'published_articles')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="consolidado.tsv"'

    writer = csv.writer(response, delimiter='	')
    writer.writerow(['letter', 'frequency'])
    writer.writerow(['John M.', '60'])
    writer.writerow(['Estefany V.', '30'])
    writer.writerow(['Julio L.', '15'])
    writer.writerow(['Martha D.', '23'])
    writer.writerow(['Esther R.', '51'])

    return response


def graph(request):

    context = RequestContext(request)
    return render_to_response('john/index.html', context)